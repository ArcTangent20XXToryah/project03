﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Engine))]
public class EngineEditor : Editor
{
    Engine controllerScript;

    void Awake()
    {
        controllerScript = (Engine)target;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        SerializedProperty waypointArray = serializedObject.FindProperty("waypoints");

        EditorGUILayout.PropertyField(waypointArray);

        if (waypointArray.isExpanded)
        {
            EditorGUILayout.PropertyField(waypointArray.FindPropertyRelative("Array.size"));

            EditorGUI.indentLevel++;

            for (int i = 0; i < waypointArray.arraySize; i++)
            {
                EditorGUILayout.PropertyField(waypointArray.GetArrayElementAtIndex(i));
            }
            EditorGUI.indentLevel--;
        }

        serializedObject.ApplyModifiedProperties();
    }
}
