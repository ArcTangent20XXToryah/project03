﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomPropertyDrawer(typeof(Waypoint))]
public class WaypointDrawer : PropertyDrawer
{
    Waypoint thisWaypoint;
    float extraHeight = 80f;
    float ExtraHeight_CONST = 90f;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        //Display the type of waypoint
        Rect waypointPosition = new Rect(position.x, position.y, position.width, 17f);
        SerializedProperty waypointType = property.FindPropertyRelative("waypointType");
        EditorGUI.PropertyField(waypointPosition, waypointType);

        switch((WaypointTypes)waypointType.enumValueIndex)
        {
            case WaypointTypes.MOVEMENT:
                Rect subtypePosition = new Rect(position.x + 17f, position.y + 17f, position.width - 17f, 17f);
                SerializedProperty movementType = property.FindPropertyRelative("moveType");
                EditorGUI.PropertyField(subtypePosition, movementType);

                //Display subtype specific information
                switch((MovementType)movementType.enumValueIndex)
                {
                    case MovementType.BEIZIER_CURVE:

                        //2 lines down
                        Rect beizierMoveTimePosition = new Rect(position.x, position.y + (17f * 2f), position.width, 17f);

                        //set up percentages
                        //position.width * .1 = 10% of the window
                        //position.width * .4 = 40% of the window
                        Rect endPointLabelPosition = new Rect(position.x, position.y + (17f * 3f), position.width * 0.2f, 17f);
                        Rect beizierEndPointPosition = new Rect(position.x + position.width * 0.2f, position.y + (17f * 3f), position.width * 0.3f, 17f);

                        Rect curvePointLabelPosition = new Rect(position.x + position.width * 0.5f, position.y + (17f * 3f), position.width * 0.2f, 17f);
                        Rect beizierCurvePointPosition = new Rect(position.x + position.width * 0.7f, position.y + (17f * 3f), position.width * 0.3f, 17f);
                        
                        SerializedProperty beizierMoveTime = property.FindPropertyRelative("beizierMoveTime");
                        SerializedProperty beizierEndPoint = property.FindPropertyRelative("beizierEndPoint");
                        SerializedProperty beizierCurvePoint = property.FindPropertyRelative("beizierCurvePoint");

                        EditorGUI.LabelField(endPointLabelPosition, "End Point:");
                        EditorGUI.LabelField(curvePointLabelPosition, "Curve Point:");
                        EditorGUI.PropertyField(beizierMoveTimePosition, beizierMoveTime);
                        EditorGUI.PropertyField(beizierEndPointPosition, beizierEndPoint, GUIContent.none);
                        EditorGUI.PropertyField(beizierCurvePointPosition, beizierCurvePoint, GUIContent.none);
                        break;
                    case MovementType.LOOK_CHAIN:
                        Rect rotatePointsPosition = new Rect(position.x, position.y + (17f * 2f), position.width, 17f);
                        Rect rotatePointsSizePosition = new Rect(position.x, position.y + (17f * 3f), position.width, 17f);

                        Rect rotatePointsElementPosition = new Rect(position.x, position.y + (17f * 4f), position.width/2, 17f);

                        Rect rotateSpeedsPosition = new Rect(position.x, position.y + (17f * 5f), position.width, 17f);

                        Rect rotateSpeedsElementPosition = new Rect(position.x + position.width / 2f, position.y + (17f * 4f), position.width/2, 17f);

                        SerializedProperty rotatePoints = property.FindPropertyRelative("rotatePoints");
                        SerializedProperty rotateSpeeds = property.FindPropertyRelative("rotateSpeeds");

                        EditorGUI.PropertyField(rotatePointsPosition, rotatePoints);
                        if (rotatePoints.isExpanded)
                        {
                            EditorGUI.PropertyField(rotatePointsSizePosition, rotatePoints.FindPropertyRelative("Array.size"));
                            rotateSpeeds.arraySize = rotatePoints.arraySize;
                            for (int i = 0; i < rotatePoints.arraySize; i++)
                            {
                                EditorGUI.PropertyField(rotatePointsElementPosition,  rotatePoints.GetArrayElementAtIndex(i));
                                EditorGUI.PropertyField(rotateSpeedsElementPosition, rotateSpeeds.GetArrayElementAtIndex(i));

                                rotatePointsElementPosition.y += 17f;
                                rotateSpeedsElementPosition.y += 17f;
                                //EditorGUILayout.PropertyField(rotatePoints.GetArrayElementAtIndex(i));

                            }
                            extraHeight = ExtraHeight_CONST + (19f * rotatePoints.arraySize);
                            
                            //extraHeight = 70 + (17f * rotatePoints.arraySize);

                        }
                        //else
                        //{
                        //    extraHeight = ExtraHeight_CONST;
                        //}
                        break;
                    case MovementType.STRAIGHT_LINE:
                        Rect endPointPosition = new Rect(position.x, position.y + (17f * 2f), position.width, 17f);
                        Rect moveTimePosition = new Rect(position.x, position.y + (17f * 3f), position.width, 17f);

                        SerializedProperty endPoint = property.FindPropertyRelative("straightEndPoint");
                        SerializedProperty moveTime = property.FindPropertyRelative("straightMoveTime");

                        EditorGUI.PropertyField(endPointPosition, endPoint);
                        EditorGUI.PropertyField(moveTimePosition, moveTime);
                        break;
                    case MovementType.WAIT:
                        Rect waitTimePosition = new Rect(position.x, position.y + (17f * 2f), position.width, 17f);
                        
                        SerializedProperty waitTime = property.FindPropertyRelative("waitTime");

                        EditorGUI.PropertyField(waitTimePosition, waitTime);
                        break;
                    default:
                        break;
                }

                break;
            case WaypointTypes.FACING:
                Rect subtypePosition2 = new Rect(position.x + 17f, position.y + 17f, position.width - 17f, 17f);
                SerializedProperty facingType = property.FindPropertyRelative("facingType");
                EditorGUI.PropertyField(subtypePosition2, facingType);
                switch ((FacingType)facingType.enumValueIndex)
                {
                    case FacingType.FREE_MOVEMENT:
                        Rect freeLookTimePosition = new Rect(position.x, position.y + (17f * 2f), position.width, 17f);
                        
                        SerializedProperty freeLookTime = property.FindPropertyRelative("freeLookTime");

                        EditorGUI.PropertyField(freeLookTimePosition, freeLookTime);
                        break;
                    case FacingType.LOOK_AND_RETURN:
                        //2 lines down
                        Rect timeToLookPosition = new Rect(position.x, position.y + (17f * 2f), position.width, 17f);

                        //set up percentages
                        //position.width * .1 = 10% of the window
                        //position.width * .4 = 40% of the window
                        Rect rotatePointXLabelPosition = new Rect(position.x, position.y + (17f * 3f), position.width * 0.1f, 17f);
                        Rect rotatePointXPosition = new Rect(position.x + position.width * 0.1f, position.y + (17f * 3f), position.width * 0.23f, 17f);

                        Rect rotatePointYLabelPosition = new Rect(position.x + position.width * 0.33f, position.y + (17f * 3f), position.width * 0.1f, 17f);
                        Rect rotatePointYPosition = new Rect(position.x + position.width * 0.43f, position.y + (17f * 3f), position.width * 0.23f, 17f);

                        Rect rotatePointZLabelPosition = new Rect(position.x + position.width * 0.66f, position.y + (17f * 3f), position.width * 0.1f, 17f);
                        Rect rotatePointZPosition = new Rect(position.x + position.width * 0.76f, position.y + (17f * 3f), position.width * 0.23f, 17f);

                        Rect timeToAndFromPosition = new Rect(position.x, position.y + (17f * 4f), position.width, 17f);

                        SerializedProperty timeToLook = property.FindPropertyRelative("timeToLook");
                        SerializedProperty rotatePointX = property.FindPropertyRelative("rotatePointX");
                        SerializedProperty rotatePointY = property.FindPropertyRelative("rotatePointY");
                        SerializedProperty rotatePointZ = property.FindPropertyRelative("rotatePointZ");
                        SerializedProperty timeToAndFrom = property.FindPropertyRelative("timeToAndFrom");

                        EditorGUI.LabelField(rotatePointXLabelPosition, "X:");
                        EditorGUI.LabelField(rotatePointYLabelPosition, "Y:");
                        EditorGUI.LabelField(rotatePointZLabelPosition, "Z:");
                        EditorGUI.PropertyField(timeToLookPosition, timeToLook);
                        EditorGUI.PropertyField(rotatePointXPosition, rotatePointX, GUIContent.none);
                        EditorGUI.PropertyField(rotatePointYPosition, rotatePointY, GUIContent.none);
                        EditorGUI.PropertyField(rotatePointZPosition, rotatePointZ, GUIContent.none);
                        EditorGUI.PropertyField(timeToAndFromPosition, timeToAndFrom);
                        break;
                    case FacingType.LOOK_CHAIN:
                        break;
                    case FacingType.FORCED_LOCATION:
                        break;
                    default:
                        break;
                }
                break;
            case WaypointTypes.EFFECT:
                break;
            default:
                break;
        }
                
        EditorGUI.EndProperty();
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return base.GetPropertyHeight(property, label) + extraHeight;
    }


}
