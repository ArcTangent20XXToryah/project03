﻿using UnityEngine;
using System.Collections;

public enum WaypointTypes
{
    MOVEMENT,
    FACING,
    EFFECT
}
[AddComponentMenu("Camera-Control/Mouse Look")]
public class Engine : MonoBehaviour
{
    public Waypoint[] waypoints;

    public enum RotationAxes { MouseXAndY = 0, MouseX = 1, MouseY = 2 }
    public RotationAxes axes = RotationAxes.MouseXAndY;
    public float sensitivityX = 15F;
    public float sensitivityY = 15F;
    public float minimumX = -360F;
    public float maximumX = 360F;
    public float minimumY = -60F;
    public float maximumY = 60F;
    float rotationY = 0F;

    IEnumerator Start()
    {
        for(int i = 0; i < waypoints.Length; i++)
        {
            switch(waypoints[i].waypointType)
            {
                case WaypointTypes.MOVEMENT:
                    yield return StartCoroutine(MovementEngine(i));
                    break;
                case WaypointTypes.FACING:
                    yield return StartCoroutine(FacingEngine(i));
                    break;
                case WaypointTypes.EFFECT:
                    break;
                default:
                    break;
            }
        }
    }

    IEnumerator MovementEngine(int waypointNum)
    {
        switch (waypoints[waypointNum].moveType)
        {
            case MovementType.BEIZIER_CURVE:
                break;
            case MovementType.LOOK_CHAIN:
                break;
            case MovementType.STRAIGHT_LINE:
                Transform endPoint = waypoints[waypointNum].straightEndPoint;
                float moveTime = waypoints[waypointNum].straightMoveTime;

                //LERP ALONG THE LINE
                float elapsedTime = 0f;
                Vector3 startPoint = transform.position;
                while(elapsedTime < moveTime)
                {
                    transform.position = Vector3.Lerp(startPoint, endPoint.position, (elapsedTime / moveTime));
                    elapsedTime += Time.deltaTime;
                    yield return new WaitForEndOfFrame();
                }

                break;
            case MovementType.WAIT:
                float waitTime = waypoints[waypointNum].waitTime;

                float elapsedWaitTime = 0f;
                while(elapsedWaitTime < waitTime)
                {
                    elapsedWaitTime += Time.deltaTime;
                    yield return new WaitForEndOfFrame();
                }
                break;
            default:
                break;
        }

        yield return null;
    }

    IEnumerator FacingEngine(int waypointNum)
    {
        switch (waypoints[waypointNum].facingType)
        {
            case FacingType.FREE_MOVEMENT:
                float freeLookTime = waypoints[waypointNum].freeLookTime;
                float elapsedTime = 0f;
                while (elapsedTime < freeLookTime)
                {
                    if (axes == RotationAxes.MouseXAndY)
                    {
                        float rotationX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * sensitivityX;

                        rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
                        rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);

                        transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0);
                    }
                    else if (axes == RotationAxes.MouseX)
                    {
                        transform.Rotate(0, Input.GetAxis("Mouse X") * sensitivityX, 0);
                    }
                    else
                    {
                        rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
                        rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);

                        transform.localEulerAngles = new Vector3(-rotationY, transform.localEulerAngles.y, 0);
                    }
                    elapsedTime += Time.deltaTime;
                    yield return new WaitForEndOfFrame();
                }    
                break;
            case FacingType.LOOK_AND_RETURN:
                float timeToLook = waypoints[waypointNum].timeToLook;
                float rotatePointX = waypoints[waypointNum].rotatePointX;
                float rotatePointY = waypoints[waypointNum].rotatePointY;
                float rotatePointZ = waypoints[waypointNum].rotatePointZ;
                float timeToAndFrom = waypoints[waypointNum].timeToAndFrom;

                //LERP ALONG THE LINE
                float elapsedTime2 = 0f;
                Quaternion startPoint = transform.rotation;
                Quaternion returnStartPoint = new Quaternion(startPoint.x, startPoint.y, startPoint.z, 0f);
                Quaternion endPoint = new Quaternion(rotatePointX, rotatePointY, rotatePointZ, 0f);
                Quaternion midPoint = new Quaternion(-rotatePointX, rotatePointY, rotatePointZ, 1f);
                while(elapsedTime2 < timeToAndFrom)
                {
                    Quaternion rotate = Quaternion.RotateTowards(startPoint, endPoint, (elapsedTime2 / timeToAndFrom));
                    transform.rotation = rotate;
                    elapsedTime2 += Time.deltaTime;
                    yield return new WaitForEndOfFrame();
                }
                elapsedTime2 = 0f;
                while(elapsedTime2 < timeToLook)
                {
                    elapsedTime2 += Time.deltaTime;
                    yield return new WaitForEndOfFrame();
                }
                elapsedTime2 = 0f;
                //midPoint = transform.rotation;
                while (elapsedTime2 < timeToAndFrom)
                {
                    Quaternion rotate = Quaternion.RotateTowards(midPoint, startPoint, (elapsedTime2 / timeToAndFrom));
                    transform.rotation = rotate;
                    elapsedTime2 += Time.deltaTime;
                    yield return new WaitForEndOfFrame();
                }
                Debug.Log(startPoint);
                break;
            case FacingType.LOOK_CHAIN:
                break;
            case FacingType.FORCED_LOCATION:
                break;
            default:
                break;
        }

        yield return null;
    }
}