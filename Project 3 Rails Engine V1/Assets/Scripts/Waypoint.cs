﻿using UnityEngine;
using System.Collections;
public enum MovementType
{
    STRAIGHT_LINE,
    BEIZIER_CURVE,
    WAIT,
    LOOK_CHAIN
}

public enum FacingType
{
    FREE_MOVEMENT,
    LOOK_AND_RETURN,
    LOOK_CHAIN,
    FORCED_LOCATION
}
[System.Serializable]
public class Waypoint
{
    /*
    Movement
    */
    public WaypointTypes waypointType;

    public MovementType moveType;

    public FacingType facingType;

    public Transform straightEndPoint;
    public float straightMoveTime;

    public float waitTime;

    public float beizierMoveTime;
    public Transform beizierEndPoint;
    public Transform beizierCurvePoint;

    public Transform[] rotatePoints;
    public float[] rotateSpeeds;


    /*
    Facings 
    */

    

    public float freeLookTime;

    public float timeToLook;
    public float rotatePointX;
    public float rotatePointY;
    public float rotatePointZ;
    public float timeToAndFrom;
    /*
    Effects
    */
}
